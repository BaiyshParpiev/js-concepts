//Prototypes

// _proto_
// Object.getPrototypeOf()

function Cat(name, color){
    this.name = name;
    this.color = color;
}

Cat.prototype.voice = function() {
    console.log(`Cat ${this.name} says miyow`)
};

const cat = new Cat('kot', 'black');
console.log(cat); //onlie object name, color
console.log(cat.__proto__); //function voice
console.log(cat.constructor); //[Function Cat]
cat.voice();




//===========
function Person() {}
Person.prototype.legs = 2;
Person.prototype.skin = 'white';

const person = new Person();
person.name = 'Vladilen';

console.log('skin' in person);  ///will seach all class than prototype
console.log('eyes' in person);  ///will seach all class than prototype

console.log(person.hasOwnProperty('name'));
console.log(person.hasOwnProperty('skin'));


//============ Object.create()

let proto = {year: 2019};
const myYear = Object.create(proto);
console.log(myYear.hasOwnProperty('year'));
console.log(myYear.__proto__ === proto);


proto.year = 2020;
proto = {year: 999}
console.log(myYear.year);
