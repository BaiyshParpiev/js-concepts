function Cat(color, name){
    this.color = color;
    this.name = name;
}

const cat = new Cat('black', 'kot');
console.log(cat);

///===

function myNew(constructor, ...args){
    const obj = {};
    Object.setPrototypeOf(obj, constructor.prototype);
    return constructor.apply(obj, args) || obj
}

const cats = myNew(Cat, "black", 'Kot');
console.log(cats);


const CaT = new Cat();

console.log(CaT);