//let

let a = 'variable a';
let b = 'variable b';


{
    a = 'new variable a'
    let b = 'local variable b'

    console.log('a', a)
    console.log('b', b)
}

console.log('a', a)
console.log('b', b)


//const

const port = 8080;
const array = ['Javascript', 'is', 'awesome'];
array.push('!');
array[0] = "JS";
console.log(array);
const obj = {};
obj.name = 'Vladilen';
obj.age = 25;

console.log(obj);

obj.age = 21;

console.log(obj);
