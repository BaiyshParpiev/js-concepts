const person = {
    surname: 'Stark',
    knows: function (what, name){
        console.log(`You ${what} know, ${name} ${this.surname}`)
    }
}

const jhon = {surname: 'Siyu'};

person.knows('all', 'Bran');
person.knows.call(jhon, 'nothing', 'Jhon')
person.knows.apply(jhon, ['nothing', 'Jhon'])
person.knows.call(jhon, ...['nothing', 'Jhon'])
person.knows.bind(jhon, 'nothing', 'Jhon')();
const bound = person.knows.bind(jhon, 'nothing', 'Jhon');
bound();



//==========

function Person(name, age) {
    this.name = name;
    this.age = age;

    console.log(this)
}

const elena = new Person('Elena', 23);


//Binding from bind()
//explaised binding
function logThis(){
    console.log(this);
}

const obi = {num: 42};
logThis.apply(obi);
logThis.call(obi);
logThis.bind(obi)();


// implased binding

const animal = {
    legs: 4,
    logThis: function () {
        console.log(this)
    }
}

animal.logThis();


// => functions

function Cat(color){
    this.color = color;
    console.log('This', this)
    ;(( ) => console.log('Arrow this ', this))()
}

new Cat('red');