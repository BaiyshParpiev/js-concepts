function sayHelloTo(name) {
    const message = 'Hello' + name;
    return function(){
        console.log(message);
    }
}

const helloToElena = sayHelloTo( ' Elena');
const helloToIgor = sayHelloTo( ' Igor');

console.log(helloToElena());
helloToIgor();


function createFrameworkManager(){
    const fw = ['Angular', 'React']

    return {
        print: function (){
            console.log(fw);
        },
        add: function (framework){
            fw.push(framework)
        }
    }
}

const manager = createFrameworkManager();
manager.print();
manager.add('VueJs');
manager.print();

console.log(manager);


//setTimeout

// const fib = [1, 2, 3, 4, 5, 8, 13];
// for(var i = 0; i < fib.length; i++){   //if var than i = undefined
//     setTimeout(function () {
//         console.log('fib ', i, ' = ' , fib[i])
//     }, 1500)
// }



const fib = [1, 2, 3, 4, 5, 8, 13];
for(var i = 0; i < fib.length; i++){
    (function(j){
        setTimeout(function () {
            console.log('fib ', j, ' = ' , fib[j])
        }, 1500)
    })(i)
}