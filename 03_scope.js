function funcA() {
    let a = 1;
    function funcB(){
        let  b = 2

        function funcC() {
            let c = 3;
            console.log('funC ', a, b, c)
        }

        funcC();
        console.log('FuncB ', a, b)
    }

    funcB()
    console.log('FuncA ', a)
}

funcA();