console.log(sum(1, 41));

function sum(a, b){
    return a + b
}

// console.log(i);
// var i = 40;    undefined no error
// console.log(i);

// console.log(num);
// const num = 23;   error before initialization
// console.log(num);


//function expression & function declaration

// console.log(square(25))   625 function declaration
//
// function square(num) {
//     return num ** 2
// }

// console.log(square(25))   function expression  error cannot access before initialization
//
// const square = (num) => {
//     return num ** 2
// }

