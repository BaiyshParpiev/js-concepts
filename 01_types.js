//null, undefined, boolean, number, string, object, symbol

console.log(typeof 0 );
console.log(typeof 'hi');
console.log(typeof true);
console.log(typeof undefined);
console.log(typeof Symbol('JS'));
console.log(typeof Math);
console.log(typeof function(){});
console.log(typeof NaN);
console.log(typeof null);

// Поведение типов

let language = 'JavaScript';

if(language){
    console.log("The best language is ", language)
}

// 0, false, '', null, undefined, NaN will give ==>  false

// String and number

console.log(1 + 2); //3
console.log(1 + "2"); //'12'
console.log("1" + "2"); //'12'
console.log('' + 1 + 0); // '10'
console.log('' - 1 + 0); // -1
console.log('3' * '8'); // 24
console.log(4 + 10 + 'px'); // '14px';
console.log("px" + 5 + 10); // 'px510';
console.log("42px" - 10); // 'NaN';
console.log(null + 10); // 10;
console.log(undefined + 10); // NaN;


// == vs ===

console.log(2 == '2') //true
console.log(2 === '2') //false
console.log(undefined == null) //true
console.log(undefined === null) //false
console.log(0 == false) //true
console.log(0 == '0') //true




